//
//  Photo.swift
//  ATPromiseKit
//
//  Created by Dejan on 01/10/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import Foundation

protocol Image {
    var imageID: String { get }
    var dateCreated: Date { get }
    var dateUpdated: Date { get }
    var description: String? { get }
    var likes: Int { get }
    var thumbURL: URL { get }
    var imageURL: URL { get }
    var author: Author { get }
}
